const fs = require("fs");
const path = require("path");

class Sorter {
  constructor(directory) {
    this.directory = directory;
  }

  createFolder(year, month, callback) {
    if (fs.existsSync(path.join(this.directory, year))) {
      if (!fs.existsSync(path.join(this.directory, year, month))) {
        fs.mkdirSync(path.join(this.directory, year, month));
      }
    } else {
      fs.mkdirSync(path.join(this.directory, year));
      fs.mkdirSync(path.join(this.directory, year, month));
    }

    callback();
  }

  readDirectory(callback) {
    fs.readdir(this.directory, (error, files) => {
      let result;

      if (!error) {
        result = files;
      } else {
        result = error;
      }

      callback(result);
    });
  }

  copyFile(filePath, year, month) {
    if (!fs.existsSync(path.join(this.directory, year, month, filePath))) {
      fs.copyFileSync(path.join(this.directory, filePath), path.join(this.directory, year, month, filePath));      
    }
  }
}

module.exports = Sorter;