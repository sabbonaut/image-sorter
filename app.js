const path = require("path");

var imagesDirectory = path.join(__dirname, "images");

const Sorter = require("./Sorter.js");
var sorter = new Sorter(imagesDirectory);

sorter.readDirectory(function(images) {
  images.forEach((image, index) => {
    let thisYear = image.substr(4, 4);
    let thisMonth = image.substr(8, 2);

    sorter.createFolder(thisYear, thisMonth, function() {
      sorter.copyFile(image, thisYear, thisMonth);
    });
  });
});